﻿using NewMarket.Database;
using NewMarket.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace NewMarket.Services
{
    public class CategoriesServices
    {
        #region Singleton
        public static CategoriesServices Instance
        {
            get
            {
                if (instence == null) instence = new CategoriesServices();

                return instence;
            }
        }

        private static CategoriesServices instence { get; set; }

        public CategoriesServices()
        {
        }
        #endregion

        public Category GetCategory(int ID)
        {
            using (var context = new NMContext())
            {
                return context.Categories.Find(ID);
            }
        }

        public int GetCategoriesCount(string search)
        {
            using (var context = new NMContext())
            {
                if (!string.IsNullOrEmpty(search))
                {
                    return context.Categories.Where(category => category.Name != null &&
                         category.Name.ToLower().Contains(search.ToLower())).Count();
                }
                else
                {
                    return context.Categories.Count();
                }
            }
        }

        public List<Category> GetCategories()
        {
            using (var context = new NMContext())
            {
                return context.Categories.ToList();
            }
        }

        public List<Category> GetCategoriesSearch(string search)
        {
            using (var context = new NMContext())
            {
                return context.Categories.Where(category => category.Name != null && category.Name.ToLower().Contains(search.ToLower())).OrderBy(x => x.ID).ToList();
            }
        }

        public List<Category> GetFeaturedCategories()
        {
            using (var context = new NMContext())
            {
                return context.Categories.Where(x => x.isFeatured && x.ImageURL != null).ToList();//.Where(x => x.isFeatured)
            }
        }

        public void SaveCategory(Category category)
        {
            using (var context = new NMContext())
            {
                context.Categories.Add(category);
                context.SaveChanges();
            }
        }
        public void UpdateCategory(Category category)
        {
            using (var context = new NMContext())
            {
                context.Entry(category).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteCategory(int ID)
        {
            using (var context = new NMContext())
            {
                var category = context.Categories.Where(x => x.ID == ID).Include(x => x.Products).FirstOrDefault();

                context.Products.RemoveRange(category.Products); //first delete products of this category
                context.Categories.Remove(category);
                context.SaveChanges();


            }
        }
    }
}
