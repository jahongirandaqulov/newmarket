﻿using NewMarket.Entities;
using NewMarket.Services;
using System;
using NewMarket.Web.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NewMarket.Web.Controllers
{
    //[Authorize(Roles ="Admin,Manager")]
    public class CategoryController : Controller
    {
        //CategoriesServices cs = new CategoriesServices();
        
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult CategoryTable()
        {
            CategorySearchViewModel model = new CategorySearchViewModel();
            
            model.Categories = CategoriesServices.Instance.GetCategories();

            /*var categories = CategoriesServices.Instance.GetCategories();
            
            if (string.IsNullOrEmpty(search) == false)
            {
                categories = categories.Where(c => c.Name != null && c.Name.ToLower().Contains(search.ToLower())).ToList();
                categories = categories.Where(c => c.Name.Contains(search)).ToList(); 
            }*/
            return PartialView("CategoryTable", model);
        }

        public ActionResult CategorySearch(string search)
        {
            CategorySearchViewModel model = new CategorySearchViewModel();
            model.SearchTerm = search;

            var totalRecords = CategoriesServices.Instance.GetCategoriesCount(search);
            model.Categories = CategoriesServices.Instance.GetCategoriesSearch(search);

            return PartialView("CategoryTable", model);
        }


        [HttpGet]
        public ActionResult Create()
        {
            NewCategoryViewModels model = new NewCategoryViewModels();
            //var categories = CategoriesServices.Instance.GetCategories();
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult Create(NewCategoryViewModels model)
        {
            if (ModelState.IsValid)
            {
                var newCategory = new Category();
                newCategory.Name = model.Name;
                newCategory.Description = model.Description;
                newCategory.ImageURL = model.ImageURL;
                newCategory.isFeatured = model.isFeatured;

                CategoriesServices.Instance.SaveCategory(newCategory);

                return RedirectToAction("CategoryTable");
            }
            else
            {
                return new HttpStatusCodeResult(500);
            }
 
        }
        [HttpGet]
        public ActionResult Edit(int ID)
        {
            EditCategoryViewModels model = new EditCategoryViewModels();
            var category = CategoriesServices.Instance.GetCategory(ID);

            model.ID = category.ID;
            model.Name = category.Name;
            model.Description = category.Description;
            model.ImageURL = category.ImageURL;
            model.isFeatured = category.isFeatured;

            return PartialView(model);
        }


        [HttpPost]
        public ActionResult Edit(EditCategoryViewModels model)
        {
            var existingCategory = CategoriesServices.Instance.GetCategory(model.ID);
            existingCategory.Name = model.Name;
            existingCategory.Description = model.Description;
            existingCategory.ImageURL = model.ImageURL;
            existingCategory.isFeatured = model.isFeatured;

            CategoriesServices.Instance.UpdateCategory(existingCategory);

            return RedirectToAction("CategoryTable");
        }

        
        [HttpPost]
        public ActionResult Delete(int ID)
        {
            CategoriesServices.Instance.DeleteCategory(ID);
            return RedirectToAction("CategoryTable");
        }
        
    }
}